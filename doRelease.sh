#!/bin/bash

COLOR=`tput setaf 3`
NC=`tput sgr0`

read -p "${COLOR}CHANGELOG.md updated?!?" changeLogUpdated
if [ -z "$changeLogUpdated" ]; then
  exit -1;
fi

echo "${COLOR}Start release process. Please wait while fetching checking for next version.${NC}";

read -p "${COLOR}Release version:${NC} " releaseVersion

git tag ${releaseVersion}
git push origin ${releaseVersion}

echo "${COLOR}DONE! ${NC}"
