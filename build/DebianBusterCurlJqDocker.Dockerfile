FROM debian:buster-slim

MAINTAINER Daniel Kellenberger daniel.kellenberger@oneo.ch

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
	    curl \
	    jq \
	    docker.io \
	&& rm -rf /var/cache/apt/archives/* \
	&& rm -rf /var/lib/apt/lists/*