# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Release 1.2.0

- Support node regex filtering (needs ConfigSeeder >= 2.16.0)

## Release 1.1.0

- Improve error handling
- Improve testing

## Release 1.0.0

Initial Release of configseeder/simpleconfigdownloader.
Feel free to provide any comments on GitLab.