FROM alpine:3.12

MAINTAINER ConfigSeeder info@configseeder.com

RUN adduser -D appuser -u 50000 && \
    apk --no-cache add curl jq bash

COPY download-config.sh /download-config.sh

RUN chown appuser /download-config.sh && \
    chmod u+x /download-config.sh

USER appuser

ENTRYPOINT [ "/bin/sh", "-c", "/download-config.sh"]