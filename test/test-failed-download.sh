#!/bin/bash

#
# Copyright (c) 2020 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
#
# Any form of reproduction, distribution, exploitation or alteration is prohibited
# without the prior written consent of Oneo GmbH.
#

should_have_no_error()
{
    # Parameter 1 is the return code
    # Parameter 2 is the text to be displayed if there is an error
  if [ "${1}" -ne "0" ]; then
    echo "ERROR # ${1} : ${2}"
    exit ${1}
  fi
}

should_have_error()
{
    # Parameter 1 is the return code
    # Parameter 2 is the text to be displayed if there is no error
  if [ "${1}" -eq "0" ]; then
    echo "ERROR # ${1} : ${2}"
    exit ${1}
  fi
}

SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
DOWNLOAD_CONFIG="${SCRIPTPATH}/../download-config.sh"
if [ -z "$TEST_CONFIGSEEDER_CLIENT_SERVERURL" ] ; then
  TEST_CONFIGSEEDER_CLIENT_SERVERURL="https://staging-postgresql-config-seeder.oneo.cloud"
fi
TEST_CONFIGSEEDER_CLIENT_APIKEY="invalid"
echo "Script located at: ${DOWNLOAD_CONFIG}"
echo "Testing against: ${TEST_CONFIGSEEDER_CLIENT_SERVERURL}"

export CONFIGSEEDER_CLIENT_TENANTKEY="configseeder"
export CONFIGSEEDER_CLIENT_SERVERURL="${TEST_CONFIGSEEDER_CLIENT_SERVERURL}"
export CONFIGSEEDER_CLIENT_APIKEY="${TEST_CONFIGSEEDER_CLIENT_APIKEY}"
export CONFIGSEEDER_CLIENT_CONFIGURATIONGROUPKEY="simpleconfigdownloader-integration-test"
export CONFIGSEEDER_CLIENT_OUTPUT_FILE="${SCRIPTPATH}/script-test.properties"
export CONFIGSEEDER_CLIENT_ACCEPT="text/x-java-properties"
export CONFIGSEEDER_CLIENT_ENVIRONMENTKEY="TEST"

${DOWNLOAD_CONFIG} >> test-failed-download.log 2>&1
grep problemCode test-failed-download.log >> test-download-config.log 2>&1
should_have_no_error $? "Problem Code should be visible"

echo "All Tests OK"
