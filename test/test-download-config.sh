#!/bin/bash

#
# Copyright (c) 2020 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
#
# Any form of reproduction, distribution, exploitation or alteration is prohibited
# without the prior written consent of Oneo GmbH.
#

should_have_no_error()
{
    # Parameter 1 is the return code
    # Parameter 2 is the text to be displayed if there is an error
  if [ "${1}" -ne "0" ]; then
    echo "ERROR # ${1} : ${2}"
    exit ${1}
  fi
}

should_have_error()
{
    # Parameter 1 is the return code
    # Parameter 2 is the text to be displayed if there is no error
  if [ "${1}" -eq "0" ]; then
    echo "ERROR # ${1} : ${2}"
    exit ${1}
  fi
}

SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
DOWNLOAD_CONFIG="${SCRIPTPATH}/../download-config.sh"
if [ -z "$TEST_CONFIGSEEDER_CLIENT_SERVERURL" ] ; then
  TEST_CONFIGSEEDER_CLIENT_SERVERURL="https://staging-postgresql-config-seeder.oneo.cloud"
fi
if [ -z "$TEST_CONFIGSEEDER_CLIENT_APIKEY" ] ; then
  TEST_CONFIGSEEDER_CLIENT_APIKEY="eyJraWQiOiJjb25maWdzZWVkZXItand0LWlkIiwiYWxnIjoiUlM1MTIifQ.eyJ0eXBlIjoiYXBpa2V5IiwidHlwZSI6ImFwaS1rZXkiLCJpc3MiOiJDb25maWdTZWVkZXJNYW5hZ2VtZW50IiwiaWF0IjoxNjAyODUyMjc3LCJleHAiOjE2NjU5NjQ4MDAsInN1YiI6IlNpbXBsZUNvbmZpZ0Rvd25sb2FkZXIgSW50ZWdyYXRpb24gVGVzdCIsImFwaS10b2tlbi1pZCI6ImU3ZTk2OTg0LTY0OTMtNDNmNS1iNjgyLWJhM2UyMjdmMDAyMSIsInRlbmFudC1pZCI6IjgyNjk5OTg3LTg0OWUtNDUxMS05ODg3LWU4ZTNkNmI0MjJlOSIsImFwaS1rZXktdHlwZSI6IkNMSUVOVCIsImFjY2VzcyI6eyJhbGxFbnZpcm9ubWVudHMiOnRydWUsImFsbENvbmZpZ3VyYXRpb25Hcm91cHMiOmZhbHNlLCJjb25maWd1cmF0aW9uLWdyb3VwcyI6W3siaWQiOiI0MmRmMmY1Yi1hYTUyLTQ2MzgtODIzZi0xMDRiZGIwOTY5MzkiLCJrZXkiOiJzaW1wbGVjb25maWdkb3dubG9hZGVyLWludGVncmF0aW9uLXRlc3QifV19fQ.drJV5yhvMttAcZUSTfu2f88iOm1TjiGJaqvCV48TyyUAZUq_bqZ3QMtuGd-6NOmkA4IGbUBdNq1rR4B8KpjrUanpUh37pg__ARLBqArPtkIyEC7QC8SfkJG9V2WwDxbDrCbb0XTskY3fnYZfhbHJftmcVagxqXOwts4eekcF4BM"
fi

echo "Script located at: ${DOWNLOAD_CONFIG}"
echo "Testing against: ${TEST_CONFIGSEEDER_CLIENT_SERVERURL}"

echo "STEP 1 - TenantKey - ServerUrl expected" >> test-download-config.log
export CONFIGSEEDER_CLIENT_TENANTKEY="configseeder"
${DOWNLOAD_CONFIG} >> test-download-config.log 2>&1
should_have_error $? "Should not be able to fetch values without CONFIGSEEDER_CLIENT_SERVERURL set"

echo "STEP 2 - ServerUrl - ApiKey expected" >> test-download-config.log
export CONFIGSEEDER_CLIENT_SERVERURL="${TEST_CONFIGSEEDER_CLIENT_SERVERURL}"
${DOWNLOAD_CONFIG} >> test-download-config.log 2>&1
should_have_error $? "Should not be able to fetch values without CONFIGSEEDER_CLIENT_APIKEY set"

echo "STEP 3 - ApiKey - ConfigurationGroupKey expected" >> test-download-config.log
export CONFIGSEEDER_CLIENT_APIKEY="${TEST_CONFIGSEEDER_CLIENT_APIKEY}"
${DOWNLOAD_CONFIG} >> test-download-config.log 2>&1
should_have_error $? "Should not be able to fetch values without CONFIGSEEDER_CLIENT_CONFIGURATIONGROUPKEY set"

echo "STEP 4 - ConfigurationGroupKey - Output File expected" >> test-download-config.log
export CONFIGSEEDER_CLIENT_CONFIGURATIONGROUPKEY="simpleconfigdownloader-integration-test"
${DOWNLOAD_CONFIG} >> test-download-config.log 2>&1
should_have_error $? "Should not be able to fetch values without CONFIGSEEDER_CLIENT_OUTPUT_FILE set"

echo "STEP 5 - Set output file - Accept header expected" >> test-download-config.log
export CONFIGSEEDER_CLIENT_OUTPUT_FILE="${SCRIPTPATH}/script-test.properties"
${DOWNLOAD_CONFIG} >> test-download-config.log 2>&1
should_have_error $? "Should not be able to fetch values without CONFIGSEEDER_CLIENT_ACCEPT set"

echo "STEP 6 - Set accept header - Environment Key expected" >> test-download-config.log
export CONFIGSEEDER_CLIENT_ACCEPT="text/x-java-properties"
${DOWNLOAD_CONFIG} >> test-download-config.log 2>&1
should_have_error $? "Should not be able to fetch values without CONFIGSEEDER_CLIENT_ENVIRONMENTKEY set"

echo "STEP 7 - Set Environment Key - Download should work without error" >> test-download-config.log
export CONFIGSEEDER_CLIENT_ENVIRONMENTKEY="TEST"
${DOWNLOAD_CONFIG} >> test-download-config.log 2>&1
should_have_no_error $? "All mandatory parameters are set. Function call should be ok"

echo "STEP 8 - Expect 'all' and 'env' present" >> test-download-config.log
grep test\.all=all ${CONFIGSEEDER_CLIENT_OUTPUT_FILE} >> test-download-config.log 2>&1
should_have_no_error $? "'test.all=all' is expected to be present in the output"
grep test\.env=env ${CONFIGSEEDER_CLIENT_OUTPUT_FILE} >> test-download-config.log 2>&1
should_have_no_error $? "'test.env=env' is expected to be present in the output"

echo "STEP 9 - Expect 'version', 'context', 'date' present" >> test-download-config.log
export CONFIGSEEDER_CLIENT_VERSION=1.1
export CONFIGSEEDER_CLIENT_CONTEXT=test
export CONFIGSEEDER_CLIENT_DATETIME="2019-07-22T08:40:00.000+02:00"
${DOWNLOAD_CONFIG} >> test-download-config.log 2>&1

grep test\.all=all ${CONFIGSEEDER_CLIENT_OUTPUT_FILE} >> test-download-config.log 2>&1
should_have_no_error $? "'test.all=all' is expected to be present in the output"
grep test\.env=env ${CONFIGSEEDER_CLIENT_OUTPUT_FILE} >> test-download-config.log 2>&1
should_have_no_error $? "'test.env=env' is expected to be present in the output"
grep test\.valid=dateTime ${CONFIGSEEDER_CLIENT_OUTPUT_FILE} >> test-download-config.log 2>&1
should_have_no_error $? "'test.valid=dateTime' is expected to be present in the output"
grep test\.version=version ${CONFIGSEEDER_CLIENT_OUTPUT_FILE} >> test-download-config.log 2>&1
should_have_no_error $? "'test.version=version' is expected to be present in the output"
grep test\.context=context ${CONFIGSEEDER_CLIENT_OUTPUT_FILE} >> test-download-config.log 2>&1
should_have_no_error $? "'test.context=context' is expected to be present in the output"

echo "STEP 10 - Expect 'other' to be the only one present" >> test-download-config.log
export CONFIGSEEDER_CLIENT_KEY_REGEX="^other\.val.*"
${DOWNLOAD_CONFIG} >> test-download-config.log 2>&1

LINES=$(wc -l ${CONFIGSEEDER_CLIENT_OUTPUT_FILE} | cut -d" " -f1)
[ "$LINES" = 3 ]
should_have_no_error $? "Only three lines expected"
grep "other\.value=is present" ${CONFIGSEEDER_CLIENT_OUTPUT_FILE} >> test-download-config.log 2>&1
should_have_no_error $? "'other.value=is present' is expected to be present in the output"

echo "All Tests OK"
